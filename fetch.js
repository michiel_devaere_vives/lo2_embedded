fetch("user.json")
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
		
for (var i = 0; i < data.length; i++) {
			var persoon = data[i];
     
      var element = document.createElement("div");
    	var elementDT = document.createElement("div");
			var elementID = document.createElement("div");
			var elementName = document.createElement("div");
			var elementClass = document.createElement("div");
      var elementEmail = document.createElement("div");
      
			element.setAttribute("class", "row");
			element.setAttribute("id", "rij");
      elementDT.setAttribute("class", "col-sm-2");
			elementID.setAttribute("class", "col-sm-2");
      elementName.setAttribute("class", "col-sm-2");
      elementClass.setAttribute("class", "col-sm-2");
      elementEmail.setAttribute("class", "col-sm-3");
      elementDT.setAttribute("style", "padding-right:20px; border-right: 1px solid #ccc;");
      elementID.setAttribute("style", "padding-right:20px; border-right: 1px solid #ccc;");
      elementName.setAttribute("style", "padding-right:20px; border-right: 1px solid #ccc;");
      elementClass.setAttribute("style", "padding-right:20px; border-right: 1px solid #ccc;");
      elementDT.setAttribute("align", "center");
      elementID.setAttribute("align", "center");
      elementName.setAttribute("align", "center");
      elementClass.setAttribute("align", "center");
      elementEmail.setAttribute("align", "center");

      elementDT.textContent = persoon.datetime;
			elementID.textContent = persoon.id;
			elementName.textContent = persoon.name;
			elementClass.textContent = persoon.klas;
      elementEmail.textContent = persoon.email;
      
			document.getElementById("tabel").appendChild(element);
			document.getElementById("rij").appendChild(elementDT);
      document.getElementById("rij").appendChild(elementID);
			document.getElementById("rij").appendChild(elementName);
			document.getElementById("rij").appendChild(elementClass);
      document.getElementById("rij").appendChild(elementEmail);
	}
});